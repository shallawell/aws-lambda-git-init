#!/usr/bin/python
from __future__ import print_function
import urllib
import urllib2
import os
import json
from pprint import pprint
from collections import OrderedDict
import sys

# get TOKEN from external var
TOKEN = os.environ['TOKEN']

remote_git = 'https://gitlab.com/api/v3/projects'
token_url = '?private_token='


# format the URL
# url = https://gitlab.com/api/v3/projects?private_token=YOURTOKENHERE
url = remote_git + token_url + TOKEN
#print(url)     #DEBUG
#print("TOKEN:", TOKEN)    #DEBUG
#print (url, data)     #DEBUG

#construct request to URL
req = urllib2.Request(url)
u = urllib2.urlopen(req)
# add headers
req.add_header('Authorization', 'token %s' % TOKEN)
req.add_header = {'Content-Type' : 'application/json' }

# use the json module to convert the 'response' to json
resp = json.loads(u.read().decode('utf-8'))

#dataDict = json.loads(resp, object_pairs_hook=JSONObject)
# use pprint to  pretty-print json
#pprint(resp, indent=4, depth=2)

#print(dataDict)

# catch the response from (req)
#response = urllib2.urlopen(req)

# use the json module to convert the 'response' to json
#results = json.load(response)

# use pprint to  pretty-print json
#pprint.pprint(results, indent=4, depth=2 )
#pprint(results)

#print(json.dumps(resp))
#print(type(resp)) # confirms type is dict  DEBUG

json1_data_dict = (resp)
#print(type(json1_data_dict))    #DEBUG

#id = json1_data_dict['id']
print('List of Projects')

for proj in json1_data_dict:
#    id = json1_data_dict[0]
#    print('id:', id)
#    print('proj:', proj)
#    print(type(proj))
    name = proj['name']
    id =  proj['id']
    http_url_to_repo = proj['http_url_to_repo']
    public = proj['public']
#    print('name:', name)
    print('Project ID: %s Name: %s Public: %s' % (id, name, public))
    print('URL: %s' % (http_url_to_repo))

#    name = proj[0]
#    print('Name of Proj:', name)

#name = json1_data_dict[]
#http_url_to_repo = json1_data_dict['http_url_to_repo']
#public = json1_data_dict['public']

#for i in id:
#    print('name:', name)



#print('The project ID is %s name is %s:' % (id, name))
#print('The HTTP origin is :', http_url_to_repo)
#print('This is a public repo,', public)

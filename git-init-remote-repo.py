#!/usr/bin/python
from __future__ import print_function
import urllib
import urllib2
import os
import json
from pprint import pprint
from collections import OrderedDict
import sys

# get TOKEN from external var
TOKEN = os.environ['TOKEN']

##### TO-DO ####################
# get name input from sys.argv
# check if name exist, before create - (bad 400 is currently the response for an existing repo)
# add initial readme (open, read file functions)
# add delete function
# add a 'init' and 'delete' option to script
#################################





#project_id = str(sys.argv[1])
remote_git = 'https://gitlab.com/api/v3/projects'
token_url = '?private_token='


# format the URL
# url = https://gitlab.com/api/v3/projects?private_token=YOURTOKENHERE
url = remote_git + token_url + TOKEN
#print(url)     #DEBUG
#print("TOKEN:", TOKEN)    #DEBUG
#print (url, data)     #DEBUG

#data
values = {
    'name': 'NewPyTest',
    'public': 'true'
}

print('url:', url)
print('data:', values)
#output data: {'name': 'NewPyTest', 'public': 'true'}

data = urllib.urlencode(values)
print('encoded values:', data)
# output encoded values: name=NewPyTest&public=true

#data = json.dumps(values)
#print('jsondump values:', json.dumps(values))
# output jsondump values: {"name": "NewPyTest", "public": "true"}

#construct request to URL
#req = urllib2.Request(url)
req = urllib2.Request(url, data=data)
u = urllib2.urlopen(url, data=data)


# add headers
req.add_header('Authorization', 'token %s' % TOKEN)
req.add_header = {'Content-Type' : 'application/json' }

# use the json module to convert the 'response' to json
resp = json.loads(u.read().decode('utf-8'))

#dataDict = json.loads(resp, object_pairs_hook=JSONObject)
# use pprint to  pretty-print json
#pprint(resp, indent=4, depth=2)

#print(dataDict)

# catch the response from (req)
#response = urllib2.urlopen(req)

# use the json module to convert the 'response' to json
#results = json.load(response)

# use pprint to  pretty-print json
#pprint.pprint(results, indent=4, depth=2 )
#pprint(results)

#print(json.dumps(resp))
#print(type(resp)) # confirms type is dict  DEBUG


#--
#json1_data_dict = (resp)
#print(type(json1_data_dict))    #DEBUG

#id = json1_data_dict['id']
#name = json1_data_dict['name']
#http_url_to_repo = json1_data_dict['http_url_to_repo']
#public = json1_data_dict['public']


#print('The project ID is %s name is %s:' % (id, name))
#print('The HTTP origin is :', http_url_to_repo)
#print('This is a public repo,', public)

#################################
# bash version  -- NEEDS TO BE ENCODED - see COMMSG
# create an initial file
# readme file vars
#EMAIL="user%40@email.com"
# Initial file name, test is just a placeholder. name of that file shouldn't change.
#FILENAME="README.md"
# use the file 'test' as the contents of ${FILENAME}
#FILECONTENTS=$(echo -ne `cat ./test` | xxd -plain | tr -d '\n' | sed 's/\(..\)/%\1/g')
# commit message
#COMMSG="new%20commit"
#echo "CREATING initial file ---"
#curl --request POST -s -H "PRIVATE-TOKEN: ${TOKEN}" \
#    "https://gitlab.com/api/v4/projects/${REPOID}/repository/files/${FILENAME}?branch=master&content=${FILECONTENTS}&commit_message=${COMMSG}" >> ${LOG}
#echo "CREATED ${FILENAME} ---"

###########
# bash version  -- NEEDS TO BE ENCODED - see COMMSG
# delete a repo
#echo "DELETING ${REPO_NAME} ---"
#curl -s -X DELETE -H "Content-Type:application/json" ${REMOTE_GIT}/api/v3/projects/${REPOID}?private_token=${TOKEN} > $CHK
#if [ `cat ${CHK}` != true ]; then
#echo "Error: Something went wrong - check ${CHK}"
#else
#echo "DELETED ${REPO_NAME} ---"

